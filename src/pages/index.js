import React from "react"
import Helmet from "react-helmet"
import Favicon from "../components/fav-nma.png"
import Container from "../components/container"
import Layout from "../components/layout"
import Top from "../components/top"
import Apply from "../components/apply"
import Checkout from "../components/checkout"
import Price from "../components/price"
import Footer from "../components/footer"

const IndexPage = () => (
  <Layout>
    <Helmet link={[
      { rel: 'shortcut icon', type: 'image/png', href: `${Favicon}` }
  ]}>
      <meta property="og:type" content="website" />
      <meta property="og:url" content="https://neutral-music.info" />
      <meta property="og:title" content="ニュートラル音楽院申し込みページ" />
      <meta property="og:description" content="ニュートラル音楽院は、正解・間違い・上手い・下手・お手本・才能といった考え方をせずに、オリジナルの楽器や楽譜をデザインし、即興演奏（アドリブ）をして曲を作ることができるようになるオンライン音楽スクールです。" />
      <meta property="og:image" content="https://neutral-music.info/images/H6-L5b.png" />
      <meta property="fb:app_id" content="951466938375474" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content="ニュートラル音楽院申し込みページ" />
      <meta name="twitter:description" content="ニュートラル音楽院は、正解・間違い・上手い・下手・お手本・才能といった考え方をせずに、オリジナルの楽器や楽譜をデザインし、即興演奏（アドリブ）をして曲を作ることができるようになるオンライン音楽スクールです。" />
      <meta name="twitter:image" content="https://neutral-music.info/images/H6-L5b.png" />
      <title>ニュートラル音楽院申し込み</title>
    </Helmet>
    <Container>
      <Top />
      <Apply />
      <div>
        <Checkout />
      </div>
      <Price />
      <Footer />
    </Container>
  </Layout>
)

export default IndexPage
