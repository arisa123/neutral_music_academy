import React from "react"
import styled from "styled-components"

const TopImg = styled.div`
  padding: 1.8rem 1.8rem 1.8rem 2rem;
`

export default ({ children }) => (
  <TopImg>
    <img style={{ width: `100%` }} src="../images/H6-L5b.png" alt="top_img" />
    {children}
  </TopImg>
)
