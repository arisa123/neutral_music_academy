import React from "react"
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import 'antd/lib/row/style';
import 'antd/lib/col/style';
import styled from "styled-components"

const PriceComponent = styled.div`
  margin: 3.1rem 0;
  padding: 0 1.8rem 0 2rem;
`

const PriceImg = styled.div`
  padding: 0;
`

const PriceContent = styled.div`
  background-color: #89BDDE;
  color: white;
  width: 100%;
  height: 29.8rem;
`

const PriceHeader = styled.h1`
  padding: 3.7rem 0 0 1.0rem;
  color: #FFFFFF;
  font-family: "YuGothic", "遊ゴシック", "Open Sans", "Helvetica Neue";
  margin: 0;
`

const PriceDetail = styled.div`
  padding: 1.5rem 2.6rem 0 2.6rem;
  font-size: 1.25rem;
`

const Yen = styled.ul`
  display: flex;
  justify-content: space-between;
  list-style: none;
  padding: 0.1rem 0;
`

const Euro = styled.ul`
  display: flex;
  justify-content: space-between;
  list-style: none;
  padding: 0.6rem 0;
`

const Note = styled.p`
  font-size: 0.95rem;
  padding: 0.5rem 0 2.5rem 2.0rem;
  font-family: "YuGothic", "遊ゴシック", "Open Sans", "Helvetica Neue";
`

const NoteAnchor = styled.a`
  color: white;
  font-family: "YuGothic", "遊ゴシック", "Open Sans", "Helvetica Neue";
  text-shadow: none;
`

export default ({ children }) => (
  <PriceComponent>
    <Row gutter={32}>
      <Col span={24}>
        <PriceImg>
          <img style={{ width: `100%`, margin: 0 }} src="../images/x4_top.jpg" alt="price_img" />
        </PriceImg>
      </Col>
      <Col span={24}>
        <PriceContent>
          <PriceHeader>レッスン料金</PriceHeader>
          <PriceDetail>
            <Yen>
              <li>音楽院 &#x2b; WSBIサロン &#x2b; 手数料&#x3a;</li>
            </Yen>
            <Euro>
              <li>$130.00 &#x2215; 月</li>
            </Euro>
          </PriceDetail>
          <Note>
            キャンセルポリシーは<NoteAnchor href="https://kazunekoyama.com/ja/x4/" target="_blank" rel="noopener noreferrer">こちら</NoteAnchor>。
          </Note>
        </PriceContent>
      </Col>
      {children}
    </Row>
  </PriceComponent>
)
