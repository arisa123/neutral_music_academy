import React from "react"
import styled from "styled-components"

const FooterComponent = styled.div`
  margin: 1.25rem 0 8rem 0;
`

const License = styled.div`
  padding: 0.5rem 4rem;
`

const LicenseLogo = styled.div`
  padding: 0 1.25rem;
  display: flex;
`

const LisenceAnchor = styled.a`
  background-image: linear-gradient(to top, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 1px, #ffffff 1px, #ffffff 2px, rgba(0, 0, 0, 0) 2px);
  padding: 0 0.2rem;
`

const LicenseParagraph = styled.div`
  padding: 0.4rem 1.25rem;
  font-size: 0.75rem;
`

const PoweredBy = styled.div`
  padding: 0.5rem 5rem;
`

const People = styled.div`
  padding: 0.5rem 0.6rem;
  font-size: 0.8rem;
`

const Policy = styled.div`
  padding: 0.5rem;
  font-size: 0.8rem;
`

const PolicyItem = styled.div`
  display: flex;
  padding: 0.1rem 0;
`

export default ({ children }) => (
  <FooterComponent>
    <License>
      <LicenseLogo>
        <LisenceAnchor href="https://creativecommons.org/licenses/by/4.0/" rel="noopener noreferrer" target="_blank">
          <img style={{ width: 30, marginBottom: 8 }} src="../images/cc.svg" alt="cc.svg" />
        </LisenceAnchor>
        <LisenceAnchor href="https://creativecommons.org/licenses/by/4.0/" rel="noopener noreferrer" target="_blank">
          <img style={{ width: 30, marginBottom: 8 }} src="../images/by.svg" alt="by.svg" />
        </LisenceAnchor>
      </LicenseLogo>
      <LicenseParagraph>
        <p>
          このサイトのコンテンツは、注記のあるものを除きすべてクリエイティブ・コモンズ 表示 4.0 国際 （作者：小山和音）でライセンスされています。
        </p>
      </LicenseParagraph>
    </License>

    <PoweredBy>
      <People>
        <p style={{ marginBottom: 0 }}>KvK number: 68350031</p>
        <p style={{ marginBottom: 0 }}>Icons: Feather by Cole Bemis [MIT Licence]</p>
        <p style={{ marginBottom: 0 }}>Icons: Daniel Bruce [Creative Commons BY-SA 4.0 Licence]</p>
      </People>
      <Policy>
        <PolicyItem>
          <img style={{ width: 18, paddingRight: 5, margin: 0 }} src="../images/eye-off.svg" alt="eye-off.svg" />
          <a href="https://www.netlify.com/privacy/" rel="noopener noreferrer" target="_blank">クッキーポリシー</a>
        </PolicyItem>
        <PolicyItem>
          <img style={{ width: 18, paddingRight: 5, margin: 0 }} src="../images/lock.svg" alt="lock.svg" />
          <a href="https://www.netlify.com/privacy/" rel="noopener noreferrer" target="_blank">プライバシーポリシー</a>
        </PolicyItem>
        <PolicyItem>
          <img style={{ width: 18, paddingRight: 5, margin: 0 }} src="../images/feather.svg" alt="feather.svg" />
          <a href="https://kazunekoyama.com/ja/terms/" rel="noopener noreferrer" target="_blank">利用規約</a>
        </PolicyItem>
        <PolicyItem>
          <img style={{ width: 18, paddingRight: 5, margin: 0 }} src="../images/shopping-cart.svg" alt="shopping-cart.svg" />
          <a href="https://kazunekoyama.com/ja/scta/" rel="noopener noreferrer" target="_blank">特定商取引法に基づく表記</a>
        </PolicyItem>
      </Policy>
    </PoweredBy>
    {children}
  </FooterComponent>
)
