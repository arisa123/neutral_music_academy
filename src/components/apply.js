import React from "react"
import styled from "styled-components"

const ApplyComponent = styled.div`
  margin: 6.25rem 0;
`

const Title = styled.h1`
  text-align: center;
  font-weight: 100;
  padding: 2.5rem 0 0 0;
  font-family: "YuGothic", "遊ゴシック", "Open Sans", "Helvetica Neue";
`

const Complete = styled.p`
  text-align: center;
  font-size: 1.1rem;
  padding: 0.75rem 0;
  font-family: "YuGothic", "遊ゴシック", "Open Sans", "Helvetica Neue";
`

export default ({ children }) => (
  <ApplyComponent>
    <Title>お申し込み</Title>
    <Complete>
      以下のボタンより決済をしていただきますと、お申し込みが完了します。
    </Complete>
    {children}
  </ApplyComponent>
)
