import React from "react"
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import 'antd/lib/row/style';
import 'antd/lib/col/style';
import styled from "styled-components"

const HeaderComponent = styled.div`
  margin: 0;
`

const Header = styled.header`
  margin-bottom: 1.5rem;
`

export default ({ children }) => (
  <HeaderComponent>
    <Header>
      <Row gutter={32} style={{ marginRight: 0 }}>
        <a style={{ display: `inline`, textShadow: `none`, backgroundImage: `none` }} href="https://kazunekoyama.com/ja/" rel="noopener noreferrer" target="_blank">
          <Col span={24}>
            <img style={{ width: 200, paddingLeft: 20, paddingTop: 32 }} src="../images/kklogo_en_med2.png" alt="logo" />
          </Col>
        </a>
      </Row>
    </Header>
    {children}
  </HeaderComponent>
)
