module.exports = {
  siteMetadata: {
    title: "Gatsby Default Starter",
  },
  plugins: [
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography.js`,
      },
    },
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-stripe-checkout`,
    `gatsby-plugin-stripe-elements`,
    {
      resolve: `gatsby-plugin-less`,
      options: {
        javascriptEnabled: true,
        modifyVars: {
          'primary-color': '#722ed1',
          'font-family': '"YuGothic", "遊ゴシック", "Open Sans", "Helvetica Neue"',
          'heading-color': '#001247',
          'primary-color': '#4ECDC4',
          'text-color': '#1B1B1B',
          'border-color-base': 'rgb(228, 228, 228)'
        }
      },
    },
  ]
}